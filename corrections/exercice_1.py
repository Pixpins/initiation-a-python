# -*- coding: utf-8 -*-
"""
@author: Melanie
"""

# Question 1
prenom = input("Quel est votre prenom?")
nom = input("Quel est votre nom?")
age = input("Quel est votre age?")

print("Bonjour {} {}, vous avez {} ans.".format(prenom, nom, age))

# Question 2
prenom = input("Quel est votre prenom?")
nom = input("Quel est votre nom?")
age = int(input("Quel est votre age?")) # On rajoute INT pour transformer le type de la variable age

if age < 18:
    print("Bonjour {} {}, vous êtes mineur".format(prenom, nom))
else:
    print("Bonjour {} {}, vous êtes majeur".format(prenom, nom))
    
# Question 3
nombreAnimaux = int(input("Combien d'animaux avez-vous?")) # On convertit en INT
for k in range(nombreAnimaux):
    nomAnimal = input("Quel est le nom de l'animal?")