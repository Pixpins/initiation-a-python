# -*- coding: utf-8 -*-
"""
@author: Melanie
"""
from random import randint

####
# Fonction

def quelNombre():
    """ Jeu ou l'utilisateur doit deviner un nombre genere par l'ordinateur"""
    cible = randint(0,100)
    proposition = -1
    
    while proposition != cible:
        proposition = int(input("Quel nombre?"))
        
        if cible < proposition:
            print("Plus petit")
        elif cible > proposition:
            print("Plus grand")
        else:
            print("Bravo!")
            

def quelNombre_limite(limite):
    """ Jeu ou l'utilisateur doit deviner un nombre genere par l'ordinateur.
    Il a un nombre limite d'essais.
    limite: int
    """
    
    cible = randint(0,100)
    proposition = -1
    nombre_essais = 0
    
    while proposition != cible and nombre_essais < limite:
        proposition = int(input("Quel nombre?"))
        nombre_essais += 1
        
        if cible < proposition:
            print("Plus petit")
        elif cible > proposition:
            print("Plus grand")
        else:
            print("Bravo!")
            
    if proposition != cible and nombre_essais == limite:
        print("Temps écoulé! Le resultat etait {}.".format(cible))
        
        
####
# Script
        
quelNombre()

quelNombre_limite(5)