# -*- coding: utf-8 -*-
"""
@author: Melanie
"""

from random import randint

####
# Fonction

def genereString():
    """ Genere un string composé de 6 chiffres aléatoires.
    -> mot : str
    """
    mot = ""
    for k in range(6):
        mot += str(randint(1,9)) # On n'oublie pas de convertir en str avant de l'ajouter à mot (qui est un str)
    return(mot)

def remplace(texte, valeur):
    """ Remplace toutes les valeurs de texte par une étoile.
    -> nouveau_mot : str
    """
    nouveau_mot = ""
    
    for l in texte:
        if l == valeur:
            nouveau_mot += "*"
        else:
            nouveau_mot += l
            
    return(nouveau_mot)

def verifieEtoile(texte):
    """ Renvoie True si texte n'est composé que d'etoiles, False sinon
    -> boolean
    """
    onlyStars = True # On suppose qu'il n'y a que des étoiles, et on cherche la preuve contraire
    for l in texte:
        if l != "*": # Si l'on trouve une preuve contre l'hypothèse de base
            onlyStars = False # On le marque
            
    return(onlyStars)

####
# Script
    
# On ecrit un jeu de pendu numerique

cible = genereString() # l'utilisateur doit deviner le mot
cible_modifiee = cible # on va rajouter les etoiles dans cible_modifiee

while not verifieEtoile(cible_modifiee): # tant qu'il n'y aura pas que des etoiles
    proposition = input("Quel chiffre?")
    cible_modifiee = remplace(cible_modifiee, proposition)
    
print("Felicitations, le mot était bien {}".format(cible))