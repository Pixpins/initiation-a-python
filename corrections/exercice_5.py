# -*- coding: utf-8 -*-
"""
@author: Melanie
"""

####
# Fonction

def laDisparition(texte):
    """ Remplace tous les e par une astérisque.
    texte: string
    -> nouveau_texte: string
    """
    nouveau_texte = ""
    
    for l in texte:
        if l == "e":
            nouveau_texte += "*"
        else:
            nouveau_texte += l
            
    return(nouveau_texte)

# Note: cette fonction peut être remplacée de façon plus simple par
# texte.replace("e", "*")
# Le but est ici de travailler sur les boucles et le parcours de texte, pour cette raison
# nous n'avons pas utilisé la fonction replace mais recodé la nôtre
    
def compteVoyelle(texte):
    """ Compte les voyelles.
    texte: string
    -> n_a, n_e, n_i, n_o, n_u, n_y : int
    """
    n_a = 0
    n_e = 0
    n_i = 0
    n_o = 0
    n_u = 0
    n_y = 0
    
    for l in texte:
        if l == "a":
            n_a += 1
        elif l == "e":
            n_e += 1
        elif l == "i":
            n_i += 1
        elif l == "o":
            n_o += 1
        elif l == "u":
            n_u += 1
        elif l == "y":
            n_y += 1
            
    return(n_a, n_e, n_i, n_o, n_u, n_y)

