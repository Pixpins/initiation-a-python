# -*- coding: utf-8 -*-
"""
@author: Melanie
"""
from random import randint

# Question 1: test de la fonction

resultat = randint(1,6)
print(resultat)

# Question 2: un peu de stats
moyenne = 0
for k in range(100): # on simule 100 lancers
    resultat = randint(1,6)
    moyenne += resultat # equivalent a "moyenne = moyenne + resultat"
moyenne /= 100 # equivalent a moyenne = moyenne/100

print("Le resultat moyen est de {}".format(moyenne))

# Testons avec 1000 lancers
moyenne = 0
for k in range(1000): # on change ici le nombre de lancers
    resultat = randint(1,6)
    moyenne += resultat 
moyenne /= 1000 # on n'oublie pas diviser par le nouveau nombre de lancers

print("Le resultat moyen est de {}".format(moyenne))

# Testons avec 100 lancers et un dé à 20 faces
moyenne = 0
for k in range(100):
    resultat = randint(1,20) # on change ici le nombre de faces
    moyenne += resultat
moyenne /= 100

print("Le resultat moyen est de {}".format(moyenne))

# Question 3: on ecrit une fonction qui nous evite de copier/coller le meme bout de code à chaque fois

def monDe(face):
    """ Simule le lancer d'un de en fonction du nombre de faces.
    face: int
    -> resultat: int
    """
    resultat = randint(1, face)
    return(resultat)

taille = 99
res = monDe(taille)
print("J'ai lance un dé à {} faces et ait fait {}".format(taille, res))

# Question 4: un peu plus de stats
n_1 = 0 # On va compter individuellement chaque resultat possible
n_2 = 0
n_3 = 0
n_4 = 0
n_5 = 0
n_6 = 0

for k in range(1000):
    res = monDe(6)
    if res == 1:
        n_1 += 1
    elif res == 2:
        n_2 += 1
    elif res == 3:
        n_3 += 1
    elif res == 4:
        n_4 += 1
    elif res == 5:
        n_5 += 1
    else:
        n_6 += 1
        
print(n_1, n_2, n_3, n_4, n_5, n_6)

# Nous verrons plus tard l'utilisation des listes, qui permettent de simplifier grandement la résolution
# de cet exercie

# Resolution bonus avec les listes
resultats = [0, 0, 0, 0, 0, 0]

for k in range(1000):
    res = monDe(6)
    resultats[res-1] += 1 # on ecrit res-1 dans l'indice car on commence à compter à 0 dans une liste

print(resultats)