\documentclass[french]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{babel}
\usepackage[a4paper, total={6in, 10in}]{geometry}

\title{Introduction à Python}
\author{Mélanie Munch (\textit{melanie.munch[at]agroparistech.fr})}
\date{Jeudi 24 Septembre 2020}

% Write code
\usepackage{listings}
\usepackage{xcolor}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}

\lstdefinestyle{mystyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{black},
    stringstyle=\color{codepurple},
    basicstyle=\ttfamily\footnotesize,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=left,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\newtheorem{theorem}{Exemple}

\usepackage{graphicx}

\usepackage{float}

\usepackage{diagbox}


\begin{document}

\maketitle

\fbox{
\begin{minipage}[c]{10.5cm}
\textbf{Objectifs du cours:}
\begin{itemize}
    \item Savoir créer un script Python
    \item Reconnaître la structure d'un script et pouvoir l'exploiter pour mieux le comprendre
    \item Savoir utiliser les variables, et connaître leurs différents types
    \item Connaître les deux types de \textbf{structures de contrôle}: \textbf{structures alternatives} \textit{IF}, \textit{ELIF}, \textit{ELSE}; et \textbf{structures itératives} \textit{WHILE}, \textit{FOR}
    \item Reconnaître les fonctions et savoir en écrire
    \item Pouvoir comprendre les messages d'erreur
\end{itemize}
\end{minipage}
}

\tableofcontents

\section{Introduction}
Pour programmer, nous avons besoin d'écrire un ensemble d'\textbf{instructions}. Cet ensemble est appelé un \textbf{script}. Afin que la machine comprenne ce que l'on souhaite réaliser, il est important d'exprimer ces instructions dans un langage précis. Par conséquent, \textbf{savoir programmer, c'est savoir utiliser ce langage et connaître sa syntaxe.}

Néanmoins, si chaque langage de programmation a sa propre syntaxe, pratiquement tous ont pour point commun d'utiliser certains mots clés en \textbf{anglais} (qui peuvent varier d'un langage à un autre). Il est donc ainsi possible de comprendre le contenu et l'objectif d'un code sans pour autant savoir le programmer.

Le script \ref{fig:example} est un exemple de script Python. Même s'il ne fait pas grand chose d'intéressant, il utilise pourtant déjà la plupart des briques de base nécessaires à la programmation d'applications bien plus complexes! A la fin de ce cours, vous aurez tous les outils vous permettant de le comprendre et de le ré-écrire à votre façon. Mais pour le moment, qu'en comprenez-vous?

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/exemple.py}
    \caption{Exemple d'un script Python}
    \label{fig:example}
\end{figure}


\begin{figure}[H]
    \centering
    \fbox{
\begin{minipage}[c]{10.5cm}
Que peut-on reconnaître? Il y a-t-il des similitudes dans la construction des instructions? Les alinéas (\textbf{indentation}) sont-ils importants? Pourquoi certains mots sont-ils colorés? Que signifient-ils?
\end{minipage}
}
\end{figure}


Et voici ce qu'il affiche une fois exécuté, en supposant que l'utilisateur ait entré \textit{"Gabriel"} comme prénom.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/exemple_sortie.py}
    \caption{Résultat affiché à l'écran après avoir exécuté le script \ref{fig:example}}
    \label{fig:example_affichage}
\end{figure}

\begin{figure}[H]
    \centering
    \fbox{
\begin{minipage}[c]{10.5cm}
Est-ce que cela correspond à ce que l'on attendait? Que se serait-il passé si on avait pris des valeurs différentes pour les variables? Pourquoi est-ce que la valeur de \textit{monChiffre} n'est pas affichée quand celle-ci vaut zéro? Que signifient les trois dernières lignes?
\end{minipage}
}
\end{figure}

\section{Les variables}
Une \textbf{variable} est un conteneur pouvant stocker de l'information. Celle-ci peut être modifiée (par l'utilisateur, l'ordinateur), et permet aux scripts de pouvoir \textbf{s'adapter}.

\begin{theorem}
Ligne 18 de l'exemple \ref{fig:example}, on définit la valeur de \textit{\textbf{monChiffre}}. Ligne 19, on la compare à la valeur 0 (\textit{"est-ce que \textbf{monChiffre} vaut 0 ou non"}); la valeur 0 n'est pas une variable, car elle vaudra toujours la même chose. En revanche, on peut modifier à volonté la valeur de \textbf{monChiffre} ligne 18.
\end{theorem}

Les variables servent de passerelles entre l'utilisateur et l'ordinateur: sans variable, un script fera toujours la même chose, et ne sera donc pas très utile.

Les variables sont très libres, et peuvent contenir beaucoup de choses de natures différentes: des dates, des textes, des listes, ... Lorsqu'une variable contient une information, on se réfère à la nature de cette information comme de son \textbf{type}.

\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|}
    \hline
        Type & Nature & Exemple \\\hline
        \textit{int} & \textit{Integer}, ou nombre entier & -5, 3, 8, 99, ... \\
        \textit{float} & Nombre flottant & -3.4, 55.98, 1028.6, ... \\
        \textit{str} & \textit{String}, ou chaîne de texte & "Bonjour", "Au revoir", "Merci", ... \\
        \textit{bool} & Booléen & True, False \\\hline
    \end{tabular}
    \caption{Principaux types utilisés pour les variables}
    \label{tab:types}
\end{table}

A noter, les booléens sont un type particulier, qui permet d'exprimer des formules logiques. Ils peuvent être combinés avec les opérateurs logiques \textbf{and} et \textbf{or} pour exprimer un nouveau booléen: $Bool_1$ OL $Bool_2 \rightarrow Bool_3$

\begin{table}[H]
    \centering
    \begin{tabular}{|c|c|c|c||c|c|c|c|}
    \hline
        Bool1 & OL & Bool2 & Bool3 &  Bool1 & OL & Bool2 & Bool3 \\\hline
        True & \textbf{and} & True & True &
        True & \textbf{or} & True & True \\
        True & \textbf{and} & False & False &
        True & \textbf{or} & False & True \\
        False & \textbf{and} & False & False &
        false & \textbf{or} & False & False \\\hline
    \end{tabular}
    \caption{Opérateurs logiques}
    \label{tab:op}
\end{table}

Il est important de savoir reconnaître un \textit{type} de variable, car les opérations possibles vont changer en fonction de celui-ci. De plus, certains types intéragissent mal: par exemple, on ne peut pas additionner un \textit{string} avec un \textit{int}. Cela peut être (très) souvent source d'erreurs, donc gardez toujours en tête ce que vos variables contiennent!

\begin{theorem}
Dans le script \ref{fig:example}, ligne 4, la fonction \textbf{format} est utilisée. Il s'agit d'une fonction particulière, réservée aux \textit{strings}, qui permet d'insérer les valeurs des variables dans la chaîne de texte à la place des accolades \{\}. \textbf{maVariable}.format(\textbf{autreVariable}) n'aurait pas de sens si le type de \textbf{maVariable} était autre chose qu'un string. En revanche, \textbf{autreVariable} peut prendre n'importe quel type.
\end{theorem}

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/variables.py}
    \caption{Exemple de manipulation de variables en fonction de leur types}
    \label{fig:variables}
\end{figure}

Python est un langage très permissif, qui autorise le changement de type d'une variable au cours d'un même script: on peut par exemple créer une variable \textit{maVar} initalement en temps que \textit{int}, puis finalement la transformer en \textit{str}. Encore une fois, afin de faciliter la compréhension de son script, il faut toujours avoir une idée de ce que représentent les variables, et de leurs types. C'est pourquoi il est important \textbf{de bien organiser son script}, de \textbf{le commenter} (en utilisant le symbole \#), et de \textbf{nommer correctement ses variables}.

En effet, une variable peut être nommée de n'importe quelle façon. Il faut néanmoins de garder à l'esprit que dans un véritable script, leur nombre peut vite grandir et avoisiner très facilement la centaine. A partir de là, il est donc nécessaire de savoir s'y retrouver: les noms des variables doivent être clairs, et les noms "temporaires" sont à proscrire (difficile de faire la différence entre \textit{toto5} et \textit{tot8984}). Enfin, pour des raisons d'encodage, il est important de ne pas utiliser d'accents afin de limiter les erreurs.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/typage.py}
    \caption{Python permet de changer les types des variables. Tout n'est cependant pas possible!}
    \label{fig:typage}
\end{figure}



\begin{figure}[H]
    \centering
    \fbox{
\begin{minipage}[c]{10.5cm}
\textbf{Conclusion}

Les variables sont des conteneurs de valeurs, pouvant prendre différent types. Ces types définissent les actions possibles avec ces variables.

Une variable peut être nommée comme on le souhaite. Néanmoins, privilégiez des noms \textbf{simples}, \textbf{indicatifs}, et \textbf{n'utilisez pas d'accents}.
\end{minipage}
}
\end{figure}

\section{Les instructions}
Chaque ligne du programme est appelée une \textbf{instruction}. Il s'agit d'un ordre que l'ordinateur va appliquer. Lorsque l'on exécute un script, l'ordinateur va passer en revue chaque ligne, en commençant par la première et en enchaînant avec les suivantes. A chaque nouvelle ligne, il va procéder à un vérification:
\begin{itemize}
    \item S'il peut l'exécuter, il passe à la suivante
    \item S'il rencontre un problème, il affiche un message d'erreur et arrête sa lecture.
\end{itemize}
Il est important de garder cela en tête: \textbf{si le script est interrompu à la ligne N, il ne vérifiera pas ce qu'il y a aux lignes N+i.}

\begin{theorem}
L'instruction de la ligne 27 du script \ref{fig:example} n'est pas appliquée, l'ordinateur n'affiche pas "J'ai fini!". Cela est dû au fait que l'instruction de la ligne 26 n'a pas pû être effectuée: on lui a demandé d'afficher une variable (\textit{monAge}) qui n'a pas été définie avant.
\end{theorem}

Les instructions sont généralement très \textbf{directes}: assigne telle valeur à telle variable, modifie-la, affiche moi le résultat.

Pour ajouter de la souplesse, il est possible d'utiliser des \textbf{structures de contrôle}, qui peuvent être de deux types: conditionnelles ou itératives. De la même façon que les variables introduisent de l'interactivité, celles-ci permettent de vérifier des conditions, et d'agir en conséquence.

\subsection{Exécution conditionnelle: \textit{if}-\textit{elif}-\textit{else}}
La structure \textit{if} exprime une condition. Elle permet de vérifier si un critère est rempli, et de décrire les actions à réaliser en fonction du résultat. Elle se structure toujours de la même façon:

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/if.py}
    \caption{Structure d'une condition \textbf{if}}
    \label{fig:if}
\end{figure}

La condition doit forcément renvoyer, à la fin de son évaluation, les valeurs \textbf{True} ou \textbf{False}; cela signifie qu'elle exprime forcément au final un booléen. Pour cela, les mot-clés \textbf{and}, \textbf{or} et \textbf{not}, ainsi que tous les opérateurs de comparaison tels qu'égal (==), inférieur (<), supérieur (>), inférieur ou égal (<=), supérieur ou égal (>=), peuvent être utilisés.

Une fois dans le \textbf{corps} de la condition, on peut indiquer les instructions que l'on attend. Celles-ci doivent toujours être précédées par une \textbf{indentation}: tant que celle-ci est présente, nous sommes toujours dans le corps de la condition, où l'on peut écrire toutes les instrcutions possibles. Il est même possible de créer une deuxième condition dans une première!

Pour terminer la boucle, il suffit de diminuer d'un cran l'indentation: c'est ce qui a été fait pour l'\textbf{instruction 3} du script \ref{fig:if}. Cela signifie donc qu'importe que \textbf{condition} soit vérifiée ou pas, \textbf{instruction 3} sera toujours exécuté.

Parfois, la boucle \textit{if} seule ne suffit pas. C'est le cas par exemple lorsque l'on veut vérifier plusieurs critères (\textit{"aimer les chiens et/ou les chats"} du test de la ligne 23 du script \ref{fig:variables} par exemple). Dans ce cas-ci, on peut alors utiliser les mots clés \textbf{elif} (qui introduit une nouvelle condition si les précédentes n'ont pas été vérfiées) et \textbf{else} (qui prend en compte tous les autres cas possibles, c'est le choix par défaut si rien n'est vérifié). 

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/elif.py}
    \caption{Structure d'une condition \textbf{if} intégrant \textbf{elif} et \textbf{else}}
    \label{fig:elif}
\end{figure}

L'intérêt ici est que l'on peut exprimer facilement plusieurs conditions. Dans ce cas-ci, l'ordinateur parcourt dans l'ordre chacune, jusqu'à trouver celle qui lui convient: à ce moment, il saute les autres conditons possibles et exécute uniquement les instructions correspondantes à la condition trouvée. Ainsi, \textbf{l'ordre dans lequel les instructions sont écrites est important}. 

\begin{theorem}
Dans le script \ref{fig:elif}, si \textbf{instruction 1} est vérifiée, l'ordinateur ne vérifiera pas le reste, et donc n'exécutera pas \textbf{instruction 2}, même si \textbf{condition 2} est également vérifiée.
\end{theorem}

Si aucune condition n'est vérifiée, les instructions rangées dans \textbf{else} (si présent) seront alors exécutées par défaut.

\subsection{Exécution itérative: \textit{while}}
Nous venons de voir la structure conditionnelle \textit{if}, qui autorise à déterminer le comportement d'un programme lorsque certaines conditions sont remplies. Néanmoins, cette structure permet seulement de spécifier de manière ponctuelle les instructions à appliquer lorsque la condition est vérifiée: comme nous l'avons vu, le script est toujours lu de haut en bas, et \textbf{une fois qu'une instruction est passée, elle ne se répète pas}. Cela peut soulever des problèmes, par exemple si l'on veut exécuter une instruction \textbf{tant qu'}une condition est vérifiée. C'est pourquoi on introduit la boucle \textit{while}.

\begin{theorem}
Dans le script \ref{fig:example}, ligne 18, on utilise la boucle \textit{while}. La ligne 19 pose la condition ("Tant que \textbf{monChiffre} est différent de zéro"), et les instructions lignes 20 et 21 sont celles à appliquer tant que la condition est vraie. Les lignes suivantes (22, 23, etc.) ne seront exécutées que lorsque l'on sera \textbf{sortis de la boucle}, c'est-à-dire que la condition ne sera plus vraie.
\end{theorem}

De la même manière qu'un \textit{if}, la boucle \textit{while} a également uns syntaxe particulière, reposant sur (1) la définition d'une condition et (2) une indentation pour marquer le corps de la boucle.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/while.py}
    \caption{Structure d'une boucle \textit{while}}
    \label{fig:while}
\end{figure}

Là encore, les \textbf{instructions 1} et \textbf{2} ne seront exécutées que si la \textbf{condition 1} est vérifiée. Néanmoins, contrairement à la structure \textit{if}, \textbf{instruction 3} ne sera exécuté que lorsque la \textbf{condition 1} ne sera plus vrai.

\textbf{Attention!!! } Une source d'erreurs \textbf{constante} dans les scripts est l'introduction de boucles \textit{while} dont la condition d'entrée est toujours vraie, et jamais modifiée:

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/while_erreur.py}
    \caption{Structure d'une boucle \textit{while} créant une boucle infinie}
    \label{fig:while_erreur}
\end{figure}

Dans le cas du script \ref{fig:while_erreur}, la condition est toujours vérifiée (car il s'agit du booléen \textit{True}). Rien ne le modifie par la suite: le programme, exécuté, va constamment afficher "0" jusqu'à extinction de la machine ou arrêt forcé du script. Il ne passera jamais à la suite, et n'affichera donc jamais "Hello world". Par conséquent, si vous êtes confrontés un jour à un script ne se terminant jamais une fois lancé, pensez à vérifier qu'il est possible de "sortir" de toutes les boucles \textit{while} présentes.

\subsection{Exécution itérative: \textit{for}}

La dernière structure de contrôle que nous présenterons ici est la boucle \textit{for}. Comme nous avons pu le voir dans le script \ref{fig:example}, ligne 23, elle permet de répéter une action autant de fois qu'on le souhaite. Ce nombre de fois est défini grâce à la fonction \textit{range}: \textit{range}(5) indique que l'on veut que l'action soit répétée cinq fois, \textit{range}(99) quatre-vingt dix-neuf fois, et \textit{range}(\textbf{monChiffre}) autant de fois que la valeur contenue dans la variable \textbf{monChiffre} (si celle-ci est de type \textit{int}).

Encore une fois, la boucle \textit{for} est définie par des structures et syntaxes lui étant propres:

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/for.py}
    \caption{Structure d'une boucle \textit{for}}
    \label{fig:for}
\end{figure}

La première chose à noter est que \textbf{maVariable} change de valeur à chaque itération, au fur et à mesure que l'on avance dans le \textit{range}: elle vaudra zéro en premier, puis un, deux, ... jusqu'à atteindre la valeur de \textbf{uneValeur}-1 (puisque l'on commence toujours par zéro). Dans le script \ref{fig:example},  cette variable (alors appelée \textbf{k}) est utilisée dans les instructions, ligne 24; mais cela n'est pas obligatoire.

Contrairement aux instructions \textit{if} et \textit{while}, le déroulement de la boucle est immuable: tant que le compte défini dans l'instruction d'entrée n'est pas fini (tant que l'on n'a pas compté autant de fois qu'indiqué), alors on ne passe pas à la suite (ici, \textbf{instruction 3}). A noter, si la valeur du range vaut zéro (i.e. on demande d'exécuter l'opération zéro fois), alors la boucle \textit{for} est ignorée.

Pour finir, la boucle \textit{for} fonctionne de manière similaire à la boucle while, et il est courant qu'elles puissent être interchangeables. Les deux reposent néanmoins sur deux philosophies différentes, que nous allons développer légèrement dans la prochaine partie.

\subsection{Quand utiliser les structures de contrôle?}
Nous venons de voir trois structures de contrôle différentes: \textit{if} (et ses dérivés, \textit{elif} et \textit{else}); \textit{while}; et \textit{for}. Si celles-ci peuvent paraître similaires, il est important de savoir distinguer les cas où leur utilisation sera la plus optimale. Voici quelques pistes de réflexions à garder en tête lorsque vous programmerez.

\begin{figure}[H]
    \centering
    \includegraphics[scale = 1]{media/diagramme.png}
    \caption{Guide à l'usage des indécis}
    \label{fig:which_one}
\end{figure}

\section{Les fonctions}
\subsection{Structure}

Une fonction en informatique est définie de la même façon qu'une fonction en mathématiques:

\begin{center}
    \textit{maFonction}(\textbf{input$_1$}, \textbf{input$_2$}, ...) = \textbf{output$_1$}, \textbf{output$_2$}, ...
\end{center}

Les inputs sont les paramètres d'entrée, les outputs les paramètres de sortie. Il peut exister des fonctions avec zéro, un ou plusieurs inputs, tout comme il peut exister des fonctions avec zéro, un ou plusieurs outputs.

Là encore, les fonctions disposent d'une structure particulière. Elle sont introduites par le mot-clé \textit{def}, et peuvent être conclues de deux façons:
\begin{itemize}
    \item par un \textit{return}, si des outputs sont présents. Attention, un \textit{return}, s'il est lu en temps qu'instruction, marque forcément l'arrêt de la fonction: même si d'autres instructions sont présentes après dans le corps de la fonction, celles-ci ne seront pas exécutées\footnote{Cela signifie que dans certains cas (comme avec des \textit{if}), il est cependant possible d'avoir plusieurs \textit{return} dans une même fonction. Ce n'est néanmoins pas une habitude de programmation que je recommande de prendre dans un premier temps!}.
    \item par aucune instruction spécifique, s'il n'y a pas d'outputs
\end{itemize}

Dans tous les cas, le corps de la fonction est là encore marqué par l'indentation. De façon similaire aux structures vues jusqu'à présent, \textbf{un retrait de l'alinéa marque dans tous les cas la fin de la fonction.}

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/function.py}
    \caption{Structure d'une fonction}
    \label{fig:function}
\end{figure}

On peut noter entre la ligne de définition et les instructions un bloc, placé entre des triple guillemets. Si ce bloc ne sert pas en temps que tel au bon déroulement de la fonction (elle marcherait très bien sans), il est néanmoins essentiel à la propreté du code, et aide à sa relecture. Il s'agit d'une habitude de programmation, qu'il est \textbf{vivement recommandé} d'acquérir. Cette description est composée de trois parties:
\begin{itemize}
    \item Une courte description du but de la fonction
    \item Les différents inputs, leur type, et ce qu'ils représentent
    \item Les différents outputs, leur type, et ce qu'ils représentent
\end{itemize}

Le but de cette \textbf{documentation} n'est pas de noyer le code sous des pavés de texte: il convient de rester concis dans la description. L'objectif ici est de donner un rapide aperçu du rôle de la fonction et de comment l'utiliser: le fonctionnement en lui-même n'a pas à être expliqué en lui même.

L'intérêt principal des fonctions est qu'elles permettent de répéter de façon efficace des morceaux de codes d'un script, sans avoir à les réécrire. \textbf{Savoir définir des fonctions utiles participe à la lisibilité de vos scripts.} Le script \ref{fig:wo_function} propose un exemple de script qui pourrait être grandement amélioré par l'utilisation de fonctions.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/wo_function.py}
    \caption{Exemple d'un script comportant des lourdeurs et des répétitions}
    \label{fig:wo_function}
\end{figure}

En effet, bien que fonctionnel, on peut noter des répétitions dans la structure: on réalise plusieurs fois la même opérations, et certaines lignes sont juste de légères variations des autres. Voici une suggestion de correction, qui ajoute une fonction.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/w_function.py}
    \caption{Script \ref{fig:wo_function} réécrit avec intégration d'une fonction}
    \label{fig:w_function}
\end{figure}

 Bien évidemment, d'autres solutions sont possibles. Néanmoins, on peut déjà noter dans cette proposition une simplification du script: il y a plus de lignes, mais une fois que le but de la fonction \textbf{maSomme} est compris (ce qui est simplifié par la présence de documentation), la compréhension globale se retrouve grandement facilitée. La documentation donne ici le but et les différents inputs et outputs: on peut noter que la fonction étant relativement simple, la description des différents paramètres n'a pas été spécifiée.
 
 \subsection{Variables et fonctions}
 Comme on peut le voir dans le script \ref{fig:w_function}, les noms des variables inputs, outputs, et ceux des variables utilisées dans les intructions varient. Cela est dû au fait qu'une fonction en informatique n'est pas écrite pour une variable précise donnée; de la même façon qu'en mathématiques une inconnue est dénotée $x$, le nom des variables est ici interne au fonctionnement de la fonction.
 \begin{itemize}
     \item Si l'on rajoutait à la ligne 20 \textit{print(\textbf{valeur1})}, cela n'aurait pas de sens, puisque cette variable n'existe que dans le corps de la fonction \textit{maSomme}
     \item De même, si l'on rajoutait ligne 9 \textit{print(\textbf{var1})}, cela n'aurait également pas de sens: \textbf{var1} n'existe pas au sein de la fonction.
 \end{itemize}

En résumé, il faut voir une fonction comme un mini-script à part du reste, \textbf{un environnement clos qui ne peut communiquer avec le reste du script que via ses inputs et ses outputs.}

\section{Messages d'erreur}
Pour terminer cette introduction, nous allons maintenant couvrir les erreurs. Lorsque l'ordinateur rencontre un problème d'exécution, il interrompt le script et affiche un message d'erreur. Si ceux-ci peuvent sembler obscurs, il est néanmoins important de savoir les comprendre: cela vous aidera grandement lors du debugage. Un message d'erreur sera généralemet toujours construit de la même façon:

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/erreur.py}
    \caption{Structure d'un message d'erreur}
    \label{fig:erreurs}
\end{figure}

Il y a quatre sections à regarder:
\begin{itemize}
    \item\textbf{Nom du fichier:} Nom du fichier du script qui a provoqué l'erreur
    \item \textbf{XX:} Numéro de la ligne dans le fichier où l'erreur a été détectée
    \item \textbf{Type de l'erreur:} Il en existe plusieurs catégories, que nous allons voir par la suite
    \item \textbf{Description de l'erreur:} Courte description en anglais du problème.
\end{itemize}

Il est important de noter que si les erreurs peuvent en théorie être nombreuses, ce sont généralement les mêmes qui reviennent (même à haut niveau!). Voici une liste non exhaustive des erreurs que vous rencontrerez probablement au moins une fois:

\begin{itemize}
    \item \textit{\textbf{SyntaxError:} invalid syntax.} Comme son nom l'indique, il s'agit d'une erreur de syntaxe. Dans ce cas-là, regardez à la ligne indiquée: il manque sûrement une parenthèse, un point, une virgule, des guillemets, deux points, un espace... Si rien ne vous choque, regardez les lignes au-dessus: parfois, l'erreur peut s'étendre sur plusieurs lignes.
    \item \textbf{\textit{IndentationError}}. Petite soeur de la précédente, elle indique un problème d'indentation.
    \item \textit{\textbf{ZeroDivisionError: }division by zero}. Vous venez de tenter une division par zéro. Oups...
    \item \textit{\textbf{NameError:} name 'X' is not defined}. Vous avez fait appel à une variable, fonction... dont le nom n'a pas été défini avant. N'oubliez pas que le script se lit de haut en bas: peut-être avez-vous défini le nom plus bas que la ligne d'erreur? Si non, alors il est probable qu'il s'agisse d'une faute de frappe: vérifiez les majuscules, l'écriture,...
    \item \textit{\textbf{TypeError:} Can't convert 'int' object to str implicitly}. Il s'agit d'une erreur arrivant lorsque vous tenter d'utiliser une méthode réservée à un certain type de variable sur un autre type. Dans cette erreur précise, une méthode de \textit{string} a été appliquée sur un \textit{integer}: par exemple, l'instruction \textit{5 + "coucou"} vous renverra cette erreur. Néanmoins, d'autres erreurs du même type sont possibles: elles seront toujours rassemblées sous la bannière du \textbf{\textit{TypeError}}. N'oubliez pas de vérifier vos types de variables: peut-être avez-vous oublié d'en convertir certains?
\end{itemize}

S'agissant ici d'un cours d'initiation, nous ne détaillerons pas plus d'erreurs. Néanmoins,
de nombreuses autres sont encore possibles: essayez de les repérer, et d'apprendre ce qu'elles représentent. Cela vous fera gagner beaucoup de temps (et vous épargnera quelques maux de têtes).

\section{Quelques conseils}

Programmer peut sembler assez complexe et obscur. Il est difficile parfois de réaliser ce que l'on fait, et l'ordinateur n'est pas toujours d'une grande aide pour s'en sortir. Il s'agit d'un exercice de rigueur, qui nécessite l'acquisition de quelques automatismes:

\begin{enumerate}
    \item Avant de se lancer dans la résolution d'un exercice, réfléchissez tout d'abord à ce que vous voulez faire et à votre objectif. \textbf{N'hésitez pas à prendre une feuille et un crayon pour prendre des notes!} De même, avant toute chose, j'encourage très vivement à commencer par résoudre "à la main" l'exercice demandé: réalisez vous-même les tâches vous fera prendre conscience de la marche à suivre, et des étapes à automatiser.
    \item La comparaison entre programmation et recette de cuisine revient souvent, mais ce n'est pas pour rien: dans les deux cas, il s'agit d'une marche à suivre, étapes par étapes. On n'enfourne pas le gâteau avant que tous les ingrédients aient été ajoutés; de même, la ligne 18 ne sera pas exécutée si la ligne 15 n'a pas déjà été parcourue.
    \item Pour filer la métaphore culinaire, on n'introduit pas en plein milieu d'une recette un ustensile qui n'a pas été défini avant; de même, on ne fait pas appel à une variable qui n'a pas été "présentée" avant.
    \item Les erreurs peuvent sembler rebutantes, mais sont source d'information. Elles vous indiqueront \textbf{toujours}: la ligne du problème, et le type de celui-ci. Savoir les exploiter vous dépannera plus d'une fois, et vous fera gagner un temps précieux.
    \item Si vous vous posez une question, c'est que vous n'êtes sûrement pas le premier à vous la poser: Internet est une source de renseignement inépuisable quand il s'agit de répondre aux questions de syntaxes, de formules, d'algorithmie, ... Si vous êtes vraiment bloqués, n'hésitez pas à chercher. Néanmoins, gardez un regard critique sur ce que vous trouvez: ne copiez/collez pas une réponse sans chercher à la comprendre, car les réponses que vous trouverez ne seront pas toujours des plus pertinentes!
    \item Les fonctions servent à éviter les répétitions. Si vous vous retrouvez à faire des copier/coller, c'est que vous avez probablement besoin de définir une fonction.
    \item En informatique, il n'y a pas de bonne ou de mauvaise réponse: il y en a juste des plus efficaces que d'autres. Savoir bien coder, ce n'est pas seulement pouvoir répondre à la question demandée; c'est également trouver la manière la plus optimisée possible.
    \item Un programme, ça s'écrit, ça se teste, ça se complète, se re-teste, se ré-écrit et se re-re-teste. N'attendez pas d'avoir écrit 200 lignes avant de vous lancer dans l'exécution: c'est le meilleur moyen de vous retrouver face à un mur d'erreurs, et de devoir changer tout ce que vous venez de produire. Au contraire, pensez régulièrement à faire des pauses dans votre codage, et à exécuter petit à petit, afin de vérifier au fil de l'écriture si tout se tient bien.
    \item La fonction \textit{help(\textbf{nomDUneFonction})} vous donnera des informations sur les fonctions usuelles. N'hésitez pas à l'utiliser dans la console pour avoir des renseignements.
    \item \textbf{FAITES DES SAUVEGARDES REGULIERES.}
\end{enumerate}

En vous souhaitant un bon début dans l'informatique, j'espère que ce poly aura sû vous donner les bases dans la programmation. Dans tous les cas, vos enseignants sont là pour répondre à vos questions!

Et n'oubliez pas l'adage: en informatique, 99\% des erreurs se situent entre la chaise et le clavier.

\section{Exercices}

\textbf{Exercice 1.} Ecrire un premier script 

\textbf{1.} Ecrire un script qui demande à un utilisateur son prénom, son nom et son âge. A partir de ces informations, lui afficher un charmant message d'accueil du type: \textit{"Bienvenue \textbf{Prénom} \textbf{Nom}, vous avez \textbf{Age} ans"}. 

\textbf{2.} Modifier le script précédent pour vérifier \textbf{automatiquement} si la personne est majeure ou mineure. Modifier le message d'accueil en conséquence pour refléter ce changement.

\textbf{3.} Demander à la personne combien d'animaux elle possède. Pour chacun, demander le nom de celui-ci. Attention, ne pas oublier que \textit{input} renvoie toujours des variables de type \textit{string}.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/sortie_exo1.py}
    \caption{Sortie attendue}
    \label{fig:sortie_attendue}
\end{figure}

\hrulefill

\textbf{Exercice 2.} Un peu de maths

\textbf{1.} Ecrire une fonction \textit{aire\_carre} qui, étant donné la longueur du côté d'un carré, renvoie son aire.

\textbf{2.} Ecrire une fonction \textit{aire\_rectangle} qui, étant donné la largeur et la longueur d'un rectangle, renvoie son aire.

\textbf{3.} Ecrire une fonction \textit{aire\_cercle} qui, étant donné le rayon d'un cercle, renvoie son aire.

\textbf{4.} Ecrire une fonction \textit{table} qui, étant donné un nombre \textbf{n} entré par l'utilisateur, renvoie les 20 premiers termes de la table de multiplication de \textbf{n}.

\textbf{5.} Ecrire une fonction \textit{divise} prenant en inputs deux nombres \textbf{a} et \textbf{b}, et qui renvoie "oui" si \textbf{a} divise \textbf{b} ou si \textbf{b} divise \textbf{a}, et non sinon. Pour savoir si deux nombres sont divisibles, vous pouvez utiliser l'opérateur modulo \% (\textbf{a}\%\textbf{b}).

\textbf{6.} Ecrire une fonction \textit{premier} qui vérifie si un nombre est premier. Pour rappel, un nombre est premier s'il n'est divisible que par 1 et par lui même. Pensez à réutiliser le résultat de la question précédente!

\hrulefill

Les prochains exercices utiliseront la fonction \textit{randint(\textbf{min}, \textbf{max})}. Celle-ci permet de générer un nombre entier aléatoire entre les valeurs \textbf{min} et \textbf{max}. Elle s'utilise ainsi:
\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/randint.py}
    \caption{Intégration de randint. La ligne 1 doit obligatoirement se situer en haut du script.}
    \label{fig:randint}
\end{figure}

\textbf{Exercice 3.} Beaucoup de dés

Cet exercice utilisera la fonction \textit{randint}. N'oubliez pas de l'importer en ajoutant en première ligne de votre script \textit{\textbf{from} random \textbf{import} randint}.

\textbf{1.} Utiliser \textit{randint} pour afficher le lancer d'un dé à 6 faces.

\textbf{2.} Nous souhaitons maintenant vérifier si la fonction simule bien un dé équilibré. Sachant que la valeur moyenne de N lancers se calcule par la formule $\frac{\textrm{somme des N lancers}}{N}$, calculer la valeur moyenne de 100 lancers de dés.

\textbf{3.} Tester avec des nombres différents de lancers. Comment pourrait-on simplement modéliser le lancer de dés différents (e.g. à 2, 8, 12, 20 faces)? Ecrire une fonction permettant de faciliter l'utilisation de votre dé intéractif.

\textbf{4.} On retourne sur un dé à 6 faces, et l'on souhaite faire des statistiques plus poussées. Afficher combien de fois chaque valeur est tombée après 1000 lancers.

\hrulefill

\textbf{Exercice 4.} A quel nombre je pense?

\textbf{1.} Développer un petit jeu dont le but est de deviner à quel nombre pense l'ordinateur. Celui-ci sera structuré de façon à ce que:
\begin{itemize}
    \item Le script génère un nombre aléatoire entre deux valeurs
    \item L'utilisateur est invité à essayer de deviner ce nombre aléatoire.
    \item En fonction de la proposition de l'utilisateur, l'ordinateur lui renvoie des indications: "Plus grand", "Plus petit"
    \item Si l'utilisateur trouve, le jeu est fini; s'il s'est trompé, le jeu continue.
\end{itemize}

\textbf{2.} Modifiez votre jeu de façon à intégrer un nombre d'essais limité pour l'utilisateur: le jeu se termine s'il trouve le bon résultat ou s'il a épuisé toutes ses chances.

\hrulefill

\textbf{Exercice 5.} Jeux de mots

Dans cet exercice, nous utiliserons le fait qu'une boucle for peut être utilisée pour parcourir une chaîne de texte.

\begin{figure}[H]
    \centering
    \lstinputlisting[language=python]{code/for_string.py}
    \caption{Ce script va afficher une à une toutes les lettres du mot.}
    \label{fig:for_string}
\end{figure}

\textbf{1.} Ecrire une fonction qui, à partir d'un mot, renvoie une nouvelle chaîne de texte où tous les "e" ont été remplacés par une astérisque.

\textbf{2.} Ecrire une fonction \textit{compteVoyelle} qui, étant donné un string donné, va compter individuellement chaque voyelle, et afficher le résultat.

\hrulefill

\textbf{Exercice 6.} Pendu numérique

\textbf{1.} Ecrire une fonction qui génère un string composé de 6 chiffres aléatoires entre 1 et 5. Exemple de sortie: "114265", "442253", ...

\textbf{2.} Ecrire une fonction qui remplace un élément donné d'une chaîne de caractère par une étoile. Exemple: \textit{remplace(\textbf{"122324"}, \textbf{"2"})} a pour résultat "1**3*4".

\textbf{3.} Ecrire une fonction qui vérifie si une chaîne de texte n'est composée que du signe *. Exemple: "***2*" renvoie \textbf{False}, "****" renvoie \textbf{True}.

\textbf{4.} A partir des fonctions codées dans cet exercice, mettez en place un jeu où un utilisateur doit deviner un \textit{mot numérique} généré aléatoirement par l'ordinateur.

\hrulefill

\begin{figure}[H]
    \centering
    \includegraphics[scale = 0.6]{media/good_code.png}
    \caption{Good code (source: https://xkcd.com/)}
\end{figure}

\end{document}
