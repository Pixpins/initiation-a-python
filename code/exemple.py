print("Hello world")

monPrenom = input("Comment vous appelez-vous?")
print("Vous vous appelez {}.".format(monPrenom))
if monPrenom == "Melanie": # Ceci est une condition IF
	print("Nous avons le meme prenom.")

valeur1 = 20
valeur2 = 40
# valeur2 = 10
if valeur1 > valeur2: # Ceci est une condition IF
	print("{} est la valeur la plus grande".format(valeur1))
elif valeur2 > valeur1:
	print("{} est la valeur la plus grande".format(valeur2))
else:
	print("Les deux valeurs sont identiques")

monChiffre = 5
while monChiffre != 0: # Ceci est une boucle WHILE
	print(monChiffre)
	monChiffre = monChiffre - 1
    
for k in range(2): # Ceci est une boucle FOR
    print("Je sais compter jusqu a {}".format(k))

print(monAge)
print("J'ai fini!")



